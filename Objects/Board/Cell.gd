extends Node2D
class_name Cell

var id : int = 0
var obstacle

var is_goal = false
var is_occupied = false

var tiles = [
	"res://Assets/Sprites/Tiles/dirt_1.png",
	"res://Assets/Sprites/Tiles/grass_1.png",
	"res://Assets/Sprites/Tiles/grass_2.png",
	"res://Assets/Sprites/Tiles/grass_3.png",
]


func _ready():
	$Label.text = str(id)
	randomize()
	var rand = randi()%4
	$Sprite.texture = load(tiles[rand])


func display_as_goal():
	$GoalDisplay.visible = true
	$SlotZone.collision_layer = 16


func show_outline():
	$Sprite.use_parent_material = false


func hide_outline():
	$Sprite.use_parent_material = true


func set_obstacle():
	$SlotZone.collision_layer = 0
	hide_outline()
	pass


func release_obstacle():
	$SlotZone.collision_layer = 8
	show_outline()
	pass
