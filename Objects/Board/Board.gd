tool
extends Node2D
class_name Board

#References
onready var astar = $Astar2D

#Ressources
var cell_res = "res://Objects/Board/Cell.tscn"

#Exports
export var height : int = 5
export var width : int = 5

export var cell_size : float = 128.0

export var offsetX : float = 64.0
export var offsetY : float = 64.0

export var step : float = 128.0
export var gap : float = 10.0

#Game logic
var _cell_id = 0

#signals
signal build_completed

func _ready():
	if !Engine.editor_hint:
		call_deferred("build_board")


func build_board():
	var cell_pos = Vector2(offsetX, offsetY)
	for h in height:
		cell_pos.y += cell_size + gap
		cell_pos.x = offsetX
		for w in width:
			cell_pos.x += cell_size + gap
			_cell_id += 1
			build_cell(_cell_id, cell_pos)
	
	emit_signal("build_completed")


func build_cell(var id: int, var cell_pos : Vector2):
	var new_cell : Cell = load(cell_res).instance()
	
	new_cell.id = id
	new_cell.position = cell_pos
	
	add_child(new_cell)

