extends Node

onready var board = get_parent()
onready var astar = AStar2D.new()

var points

# Called when the node enters the scene tree for the first time.
func _ready():
	board.connect("build_completed", self,"_on_board_build_completed")


func get_cells() -> Array:
	var cells = []
	for child in board.get_children():
		if child is Cell:
			cells.append(child)
	return cells


func _set_connections():
	for point in astar.get_points():
		#Si il y a encore une colonne à la gauche du point
		if point % board.width != 0:
			astar.connect_points(point, point + 1)
		#Si il y a encore une ligne en dessous du point
		if point + board.width <= board.height * board.width: 
			astar.connect_points(point, point + board.width)
	
#	_print_connections()


func disconnect_cell(var id : int):
	if astar.has_point(id):
		#Check if there is a cell that exists
		var neightboor_cells = [id + 1,id - 1,id - board.width,id + board.width]
		for neighboor in neightboor_cells:
			if astar.has_point(neighboor) && astar.are_points_connected(id, neighboor):
				astar.disconnect_points(id, neighboor)


func connect_cell(var id : int):
	if astar.has_point(id):
		
		#Set neightboor cells
		var neightboor_cells
		if id % board.width == 0: #Cell is at the border right
			neightboor_cells = [id - 1,id - board.width,id + board.width]
		elif id % board.width == 1: #Cellis at the border left
			neightboor_cells = [id + 1,id - board.width,id + board.width]
		else: #Else no border
			neightboor_cells = [id + 1, id - 1,id - board.width,id + board.width]
		
		for neighboor in neightboor_cells:
			if astar.has_point(neighboor):
				astar.connect_points(id, neighboor)



func get_closest_point(var to_position : Vector2):
	return astar.get_closest_point(to_position)


func get_point_path(var from_id : int, var to_id : int):
	return astar.get_point_path(from_id, to_id)


func _print_connections():
	for point in astar.get_points():
		print(point," connected to : ", astar.get_point_connections(point))



func _on_board_build_completed():
	points = get_cells()
	for point in points:
		astar.add_point(point.id, point.global_position)
	
	_set_connections()


