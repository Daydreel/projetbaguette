extends Node2D
class_name Goal

var board : Board
var astar

var goal_point

# Called when the node enters the scene tree for the first time.
func _ready():
	visible = false
	board = get_tree().get_nodes_in_group("board")[0]
	astar = board.astar
	call_deferred("set_goal_point")


func set_goal_point():
	goal_point = astar.get_closest_point(global_position)
	_display_heart_of_forest()


func _display_heart_of_forest():
	for cell in board.get_children():
		if cell is Cell && cell.id == goal_point:
			cell.is_goal = true
			cell.display_as_goal()
