extends Node2D

#References
export(String, FILE, "*.tscn") var object_np

var board : Board
var goal : Goal

onready var timer = $Timer

#Game Logic
export var number := 2
export var cooldown := 3.0

# Called when the node enters the scene tree for the first time.
func _ready():
	board = get_tree().get_nodes_in_group("board")[0]
	goal = get_tree().get_nodes_in_group("goal")[0]
	timer.wait_time = cooldown
	call_deferred("spawn")


func spawn():
	if number > 0:
		var object = load(object_np).instance()
		object.global_position = global_position
		object.board = board
		object.goal = goal
		get_parent().add_child(object)
		
		number -= 1
		
	if number > 0:
		timer.start()


func _on_Timer_timeout():
	spawn()
	pass # Replace with function body.
