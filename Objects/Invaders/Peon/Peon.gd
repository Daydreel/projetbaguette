extends KinematicBody2D

#References
var board
var goal
onready var tween := $Tween
onready var life_bar := $ProgressBar

#Export & game logic
var is_on_goal = false

var max_life_force : float = 100.0
var life_force := max_life_force

export var life_span = GameS.forest_power

func _ready():
	GameS.nbr_invaders += 1
	life_bar.value = 100
	what_s_that_feel()


func what_s_that_feel():
	tween.interpolate_property(self,"life_force",
	max_life_force,0.0,life_span,
	Tween.TRANS_LINEAR,Tween.EASE_IN)
	tween.start()


func die():
	var object = load("res://Objects/Invaders/deathAnim.tscn").instance()
	object.global_position = global_position
	get_parent().add_child(object)
	GameS.nbr_invaders -= 1
	queue_free()


func _on_Tween_tween_step(object, key, elapsed, value):
	var life_per = (value/max_life_force) * 100
	life_bar.value = life_per
	if value == 0.0:
		die()


func _on_GoalDetector_area_entered(area):
	if area.get_parent() is Cell:
		var cell = area.get_parent()
		if cell.is_goal:
			is_on_goal = true
