extends Node

#References
onready var body : KinematicBody2D = get_parent()
onready var sprite : AnimatedSprite = body.get_node("AnimatedSprite")
onready var board : Board = body.board
onready var astar = board.astar
onready var goal : Goal = body.goal
onready var goal_point = goal.goal_point
#Exports
export var speed = 100

#Game Logic
var target = Vector2()
var motion
var path = Array()


func _ready():
	set_path()

func _physics_process(delta):
	move_to_target()


func move_to_target():
	var dir = body.global_position.direction_to(target)
	var dis = body.global_position.distance_to(target)
	if dis >= 5.0:
		motion = speed * dir
		body.move_and_slide(motion)
	else:
		next_target()
	
	_flip(dir)


func _flip(var dir):
	if dir.x >= 0:
		sprite.flip_h = true
	else:
		sprite.flip_h = false

func next_target():
	if path.empty():
		if body.is_on_goal:
			goal_reached()
		else:
			print("I'm stuck !")
			yield(get_tree().create_timer(1.0), "timeout")
			set_path()
	else: 
		target = path[0]
		path.remove(0)


func get_path():
	var first_point = astar.get_closest_point(body.global_position)
	return astar.get_point_path(first_point, goal_point) #Change 9 to goal

func set_path():
	path = get_path()
	next_target()


func goal_reached():
	GameS.lose_condition()
	set_physics_process(false)
