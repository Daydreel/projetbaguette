extends Area2D

onready var body : KinematicBody2D = get_parent()
onready var board : Board = body.board
onready var astar = board.astar
onready var move = body.get_node("Move")

func _on_ObstacleDetector_body_entered(body):
	if body.is_in_group("obstacles"):
		move.set_path()
