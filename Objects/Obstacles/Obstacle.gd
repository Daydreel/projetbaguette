extends KinematicBody2D
class_name Obstacle

var board : Board
var astar

#time logic
var t = 0

export var is_movable = true 
var is_selected = false
var cell_hover : Cell

func _ready():
	board = get_tree().get_nodes_in_group("board")[0]
	astar = board.astar


func _input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("right_clic"):
		if is_selected:
			deselect()
		elif is_movable:
			select()


func _process(delta):
	t += 1
	t = clamp(t,0,100)
	t %= 100
	if !is_movable && t < 0:
		astar.disconnect_cell(cell_hover.id)


func select():
	MouseS.obstacle_selected = self
	is_selected = true
	collision_layer = 2
	
	$Sprite.z_index = 5
	
	if cell_hover:
		cell_hover.show_outline()
		astar.connect_cell(cell_hover.id)
		cell_hover.release_obstacle()


func deselect():
	if cell_hover:
		MouseS.obstacle_selected = null
		is_selected = false
		global_position = cell_hover.global_position
		collision_layer = 34
		astar.disconnect_cell(cell_hover.id)
		
		$Sprite.z_index = 1
		
		cell_hover.set_obstacle()


func _on_CellDetector_area_entered(area):
	if area.get_parent() is Cell:
		var previous_cell = cell_hover
		cell_hover = area.get_parent()
		#If the obstacle is on a cell in the begining, stick it to the cell
		if is_selected == false:
			deselect()
		else:
			if previous_cell:
				previous_cell.hide_outline() #Hide previous cell
			if !cell_hover.is_occupied:
				cell_hover.show_outline()



func _on_CellDetector_area_exited(area):
	if area.get_parent() is Cell:
		var previous_cell = area.get_parent()
		previous_cell.hide_outline()


func _on_Obstacle_mouse_entered():
	$Sprite.use_parent_material = false #Outline the sprite


func _on_Obstacle_mouse_exited():
	$Sprite.use_parent_material = true #Don't outline the sprite
