extends Node

var win_np = "res://GUI/Win.tscn"
var lose_np = "res://GUI/Lose.tscn"
var i_level : int = 0

var levels = [
	"res://GUI/Screens/StartScreen/StartScreen.tscn",
	"res://GUI/Screens/1_Screen/TitleScreen.tscn",
	"res://Levels/1_level.tscn",
	"res://GUI/Screens/2_Screen/2_Screen.tscn",
	"res://Levels/2_level.tscn",
	"res://Levels/3_level.tscn",
	"res://GUI/Screens/3_Screen/3_Screen.tscn",
	"res://Levels/4_level.tscn",
	"res://Levels/5_level.tscn",
	"res://GUI/Screens/4_Screen/4_Screen.tscn",
	"res://Levels/6_level.tscn",
	"res://Levels/7_level.tscn",
	"res://GUI/Screens/5_Screen/5_Screen.tscn",
	"res://Levels/8_level.tscn",
	"res://Levels/9_level.tscn",
	"res://GUI/Screens/6_Screen/6_Screen.tscn",
	"res://Levels/10_level.tscn",
	"res://GUI/Screens/VictoryScreen/VictoryScreen.tscn"
]

var current_level := "res://Levels/1_level.tscn"

#Duration before peon transform into awesome squirell, ready to live a nut life !
export var forest_power := 20.0

#Spawnees/Invaders
var nbr_invaders = 0 setget set_nbr_invaders


func set_nbr_invaders(new_value):
	nbr_invaders = new_value
	if nbr_invaders == 0:
		win_condition()


func win_condition():
	$Wait.start(2.0)
	yield($Wait,"timeout")
	get_tree().change_scene("res://GUI/Win.tscn")
	
	$Wait.start(1.5)
	yield($Wait,"timeout")
	next_level()


func lose_condition():
	get_tree().change_scene("res://GUI/Lose.tscn")
	$Wait.start(1.5)
	yield($Wait,"timeout")
	restart_level()


func next_level():
	i_level += 1
	current_level = levels[i_level]
	get_tree().change_scene(levels[i_level])


func restart_level():
	nbr_invaders = 0
	get_tree().change_scene(current_level)
